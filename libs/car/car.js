$(document).ready(function () {
    // Tabs - end
    ///// Car animate - start /////
    if($(window).width() > 480) {
        var carpath = {
            road1: {
                curviness: 0,
                autoRotate: true,
                force3D: true,
                type: "cubic",
                values: [{
                    x: 162,
                    y: 0
                }, {
                    x: 153,
                    y: 16
                }, {
                    x: 145,
                    y: 28
                }, {
                    x: 139,
                    y: 46
                }, {
                    x: 137,
                    y: 100.1
                }]
            },
            point1: {
                curviness: 0,
                autoRotate: true,
                force3D: true,
                type: "cubic",
                values: [{
                    x: 137,
                    y: 100.2
                }, {
                    x: 137,
                    y: 100
                }, {
                    x: 137,
                    y: 100.5
                }, {
                    x: 137,
                    y: 100.5
                }]
            },
            road2: {
                curviness: 0,
                autoRotate: true,
                force3D: true,
                values: [{
                    x: 137,
                    y: 100.5
                }, {
                    x: 138,
                    y: 131
                }, {
                    x: 140,
                    y: 150
                }, {
                    x: 143,
                    y: 170
                }, {
                    x: 152,
                    y: 215
                }]
            },
            point2: {
                curviness: 0,
                autoRotate: true,
                force3D: true,
                type: "cubic",
                values: [{
                    x: 152,
                    y: 215
                }, {
                    x: 152,
                    y: 215
                }, {
                    x: 152.3,
                    y: 216
                }, {
                    x: 152.3,
                    y: 216
                }]
            },
            road3: {
                curviness: 0,
                autoRotate: true,
                force3D: true,
                values: [{
                    x: 152.3,
                    y: 216
                }, {
                    x: 156,
                    y: 236
                }, {
                    x: 163,
                    y: 260
                }, {
                    x: 170,
                    y: 285
                }, {
                    x: 183,
                    y: 340
                }]
            },
            point3: {
                curviness: 0,
                autoRotate: true,
                force3D: true,
                type: "cubic",
                values: [{
                    x: 183,
                    y: 340
                }, {
                    x: 183,
                    y: 340
                }, {
                    x: 183.5,
                    y: 341.5
                }, {
                    x: 183.5,
                    y: 341.5
                }]
            },
            road4: {
                curviness: 0,
                autoRotate: true,
                force3D: true,
                values: [{
                    x: 183.5,
                    y: 341.5
                }, {
                    x: 190,
                    y: 367
                }, {
                    x: 193,
                    y: 390
                }, {
                    x: 200,
                    y: 435
                },
                    {
                        x: 202,
                        y: 492
                    }]
            },
            point4: {
                curviness: 0,
                autoRotate: false,
                force3D: true,
                type: "cubic",
                values: [{
                    x: 202,
                    y: 492
                }, {
                    x: 202,
                    y: 492
                }, {
                    x: 202,
                    y: 492.1
                }, {
                    x: 201.8,
                    y: 492.1
                }]
            }

        };
    }else{
        var carpath = {
            road1: {
                curviness: 0,
                autoRotate: true,
                force3D: true,
                type: "cubic",
                values: [{
                    x: 68,
                    y: 39
                },  {
                    x: 67,
                    y: 39
                }, {
                    x: 66,
                    y: 39
                }, {
                    x: 65,
                    y: 39
                }, {
                    x: 64,
                    y: 39
                }]
            },
            point1: {
                curviness: 0,
                autoRotate: true,
                force3D: true,
                type: "cubic",
                values: [{
                    x: 64.1,
                    y: 39
                }, {
                    x: 64.1,
                    y: 39
                }, {
                    x: 64.2,
                    y: 38
                }, {
                    x: 64.2,
                    y: 38
                }]
            },
            road2: {
                curviness: 0,
                autoRotate: true,
                force3D: true,
                values: [{
                    x: 64.1,
                    y: 39
                }, {
                    x: 66,
                    y: 61
                }, {
                    x: 68,
                    y: 70
                }, {
                    x: 71,
                    y: 86
                },   {
                    x: 74,
                    y: 101
                }]
            },
            point2: {
                curviness: 0,
                autoRotate: true,
                force3D: true,
                type: "cubic",
                values: [{
                    x: 74,
                    y: 101
                }, {
                    x: 74,
                    y: 101
                }, {
                    x: 74.3,
                    y: 102
                }, {
                    x: 74.3,
                    y: 102
                }]
            },
            road3: {
                curviness: 0,
                autoRotate: true,
                force3D: true,
                values: [{
                    x: 74.3,
                    y: 101
                }, {
                    x: 79,
                    y: 116
                }, {
                    x: 84,
                    y: 134
                }, {
                    x: 89,
                    y: 154
                }, {
                    x: 92,
                    y: 170
                }]
            },
            point3: {
                curviness: 0,
                autoRotate: true,
                force3D: true,
                type: "cubic",
                values: [{
                    x: 92,
                    y: 170
                }, {
                    x: 92,
                    y: 170
                }, {
                    x: 92.1,
                    y: 171
                }, {
                    x: 92.1,
                    y: 171
                }]
            },
            road4: {
                curviness: 0,
                autoRotate: true,
                force3D: true,
                values: [{
                    x: 92.5,
                    y: 170
                }, {
                    x: 97,
                    y: 186
                }, {
                    x: 100,
                    y: 208
                }, {
                    x: 102,
                    y: 230
                },
                    {
                        x: 102,
                        y: 250
                    }]
            },
            point4: {
                curviness: 0,
                autoRotate: false,
                force3D: true,
                type: "cubic",
                values: [{
                    x: 102,
                    y: 250
                }, {
                    x: 102,
                    y: 250
                }, {
                    x: 102.1,
                    y: 250
                }, {
                    x: 102.1,
                    y: 250
                }]
            }

        };
    }
        // Init controller
        var controller = new ScrollMagic.Controller();

        // Create tween
        var tween = new TimelineMax()

            .add(TweenMax.to($(".car"), 0.01, {
                css: {
                    bezier: carpath.road1,
                    display: "none"
                },
                ease: Power4.easeOut
            }))
            .add(TweenMax.to($(".car"), 1.5, {
                css: {
                    bezier: carpath.point1,
                    display: "block"
                },
                ease: Expo.easeIn,
                onUpdate: popoverShow,
                onComplete: popoverHide,
                onReverseComplete: popoverHide
            }))


            .add(TweenMax.to($(".car"), .7, {
                css: {
                    bezier: carpath.road2,
                    display: "block"
                },
                ease: Power0.easeOut
            }))
            .add(TweenMax.to($(".car"), 1, {
                css: {
                    bezier: carpath.point2
                },
                ease: Expo.easeIn,
                onUpdate: popoverShow2,
                onComplete: popoverHide2,
                onReverseComplete: popoverHide2
            }))

            .add(TweenMax.to($(".car"), .7, {
                css: {
                    bezier: carpath.road3,
                    display: "block"
                },
                ease: Power0.easeOut
            }))
            .add(TweenMax.to($(".car"), 1, {
                css: {
                    bezier: carpath.point3
                },
                ease: Expo.easeIn,
                onUpdate: popoverShow3,
                onComplete: popoverHide3,
                onReverseComplete: popoverHide3
            }))

            .add(TweenMax.to($(".car"), .7, {
                css: {
                    bezier: carpath.road4,
                    display: "block"
                },
                ease: Power0.easeOut
            }))
            .add(TweenMax.to($(".car"), 1, {
                css: {
                    bezier: carpath.point4
                },
                ease: Expo.easeIn,
                onUpdate: popoverShow4,
                onComplete: popoverHide4,
                onReverseComplete: popoverHide4
            }));


        // Build scene
        var scene = new ScrollMagic.Scene({
            triggerElement: "#trigger",
            duration: 400,
            tweenChanges: true
        })
            .setTween(tween)
            .addTo(controller);

        // tween.reverse();

        var lastScrollTop = 0;

        function popoverShow() {
            $(".popover1").find('img').attr('src', 'img/road_item_active.png');

        };

        function popoverHide() {
            $(".popover1").find('img').attr('src', 'img/road_item1.png');

        };

        function popoverShow2() {
            $(".popover2").find('img').attr('src', 'img/road_item2_active.png');

        };

        function popoverHide2() {
            $(".popover2").find('img').attr('src', 'img/road_item2.png');

        };

        function popoverShow3() {
            $(".popover3").find('img').attr('src', 'img/road_item3_active.png');

        };

        function popoverHide3() {
            $(".popover3").find('img').attr('src', 'img/road_item3.png');

        };

        function popoverShow4() {
            $(".popover4").find('img').attr('src', 'img/road_item4_active.png');

        };

        function popoverHide4() {
            $(".popover4").find('img').attr('src', 'img/road_item4.png');

        };

    ///// Car animate - end /////
});




var lastScrollTop = 0;
$(window).scroll(function (event) {
    var st = $(this).scrollTop();
    if (st > lastScrollTop) {
        $(".car").attr("src", "img/animated_car_up.png"); //right

    } else {
        $(".car").attr("src", "img/animated_car_up2.png"); //left
    }
    lastScrollTop = st;
});
