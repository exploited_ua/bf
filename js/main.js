(function() {

    [].slice.call( document.querySelectorAll( '.tabs' ) ).forEach( function( el ) {
        new CBPFWTabs( el );
    });

})();
var callback = document.getElementById("callback");
var btn = document.getElementById("open_modal");
var span = document.getElementById("close");
var body = document.getElementById("body");

btn.onclick = function () {
    callback.style.display = "block";
}

span.onclick = function () {
    callback.style.display = "none";
}

window.onclick = function (event) {
    if (event.target == callback) {
        callback.style.display = "none";
    }
    else if (event.target == success_callback) {
        success_callback.style.display = "none";
    }
    else if (event.target == callback_footer) {
        success_callback.style.display = "none";
    }

}

var open_success = document.getElementById("open_success");
var success_callback = document.getElementById("success_callback");
var close2 = document.getElementsByClassName("close2")[0];

open_success.onclick = function () {
    success_callback.style.display = "block";
}
close2.onclick = function () {
    success_callback.style.display = "none";
}

var callbackf = document.getElementById("callback_footer");
var btnf = document.getElementById("open_modal_footer");
var spanf = document.getElementById("close_footer");

btnf.onclick = function () {
    callbackf.style.display = "block";
    event.preventDefault();
}

spanf.onclick = function () {
    callbackf.style.display = "none";
}

$(document).ready(function () {
    $(".dropdown_item").on("click", function (event) {
        event.preventDefault();
        $(this).parent().find(".dropdown-menu").toggleClass("show");
        $(this).toggleClass("open");
    });
    $(".open_menu").on("click", function () {
        $(".menu").toggleClass("show");
    });

    $('.top_slider').slick({
        dots: true
    });
    $('.bought_car_slider').slick({
        slidesToShow: 3,
        slidesToScroll: 3,
        responsive: [
            {
                breakpoint: 1020,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    infinite: true
                }
            },
            {
                breakpoint: 642,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
            // {
            //     breakpoint: 480,
            //     settings: {
            //         slidesToShow: 1,
            //         slidesToScroll: 1
            //     }
            // }
    });
    $('.partners_slider').slick({
        slidesToShow: 5,
        slidesToScroll: 3,
        responsive: [
            {
                breakpoint: 1020,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true
                }
            },
            {
                breakpoint: 640,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    infinite: true
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: true
                }
            }
        ]
    });


    var phone_copy1 = $('.phone.free').html();
    var phone_copy2 = $('.phone.evaluation').html();

    $('.phone_block_wrap').html(phone_copy1 + phone_copy2);

    $('.car_item.change').on('mouseenter', function () {
        $('.car_item.change').removeClass('active');
        $(this).addClass('active');
    });
    $('.car_item.change').on('mouseleave', function () {
        $(this).removeClass('active');
    });

    $('.car_item.new').on('mouseenter', function () {
        $('.car_img').css('background-image', 'url(img/new_car.png)');
    });

    $('.car_item.previously_used').on('mouseenter', function () {
        $('.car_img').css('background-image', 'url(img/previously-used_1.png)');
    });
    $('.car_item.broken').on('mouseenter', function () {
        $('.car_img').css('background-image', 'url(img/broken_car.png)');
    });
    $('.car_item.moscow').on('mouseenter', function () {
        $('.car_item.moscow').toggleClass('active');
        $('.redeemable_car').toggleClass('moscow');
    });
    $('.car_item.region').on('mouseenter', function () {
        $('.car_img').css('background-image', 'url(img/region.png)');
        $('.car_item.moscow').removeClass('active');
        $('.redeemable_car').removeClass('moscow');
    });

    $(".open_sub_item").on("click", function (event) {
        event.preventDefault();
        $(this).parent().find(".sub-menu:first").toggleClass("open");
        $(this).toggleClass("open");
    });
    $('.back_menu').on('click', function () {
        $(this).parent('.sub').removeClass('open');
        from_back_menu = true;
    });

    $("#form").submit(function(e) {
        e.preventDefault();
        var check = 1;
        $('#form').find('.rfield').each(function() {
            if( $(this).val() == '' ) {
                $(this).attr("placeholder", "Пожалуйста заполните это поле").addClass('warning');
                check = 0;
            }
        });
        if ( check == 1 ) {
            $.ajax({
                type: "POST",
                url: "./mail.php",
                data: $(this).serialize()
            }).done(function() {
                $(this).find("input").val("");
                $("#form").trigger("reset");
                $("#open_success").trigger("click");
            });
            return false;
        }
    });
});
